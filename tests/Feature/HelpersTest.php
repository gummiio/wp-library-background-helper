<?php

namespace Test\Feature;

use Gummiforweb\WpBackgroundHelper\WpBackgroundHelper;
use Tests\TestCase;
use Tests\WpUploadImage;

class HelpersTest extends TestCase
{
    use WpUploadImage;

    public function setUp()
    {
        global $wp_bg_helper_cache;

        parent::setUp();

        $wp_bg_helper_cache = [];

        add_image_size('test_480', 480, null, false);
        add_image_size('test_640', 640, null, false);
        add_image_size('test_768', 768, null, false);
        add_image_size('test_1024', 1024, null, false);
        $this->regenerateThunbmails(static::$imageId);

        WpBackgroundHelper::clearCache();
        WpBackgroundHelper::setScreenMaxWidth(1800);
        WpBackgroundHelper::setThumbnailMaxSize('safe_full');
        WpBackgroundHelper::setBreakpointShorthands([
            'xs' => 320, 'sm' => 640, 'md' => 768, 'lg' => 980, 'xl' => 1200, 'xxl' => 1400
        ]);
        WpBackgroundHelper::setBreakpointPresets([
            'with-value' => [320 => 1, 640 => 2, 768 => 3],
            'with-shorthand' => ['xs' => 1, 'sm' => 2, 'md' => 3, 'lg' => 4],
            'with-value-string' => '320:1, 640:2, 768:3, 980:4+',
            'with-shorthand-string' => 'xs:1, sm:2, md:3, lg:4+'
        ]);
    }

    /** @test */
    public function it_generate_a_unique_id_if_no_selector_passed()
    {
        global $wp_bg_helper_cache;

        ob_start();
        wp_bg_helper(static::$imageId);

        $uniqueId = $wp_bg_helper_cache? array_keys($wp_bg_helper_cache)[0] : null;

        $this->assertCount(1, $wp_bg_helper_cache);
        $this->assertEquals(sprintf('data-bg="%s"', $uniqueId), ob_get_clean());
    }

    /** @test */
    public function it_will_generate_unique_id_with_proper_presudo_if_no_selector_passed()
    {
        global $wp_bg_helper_cache;

        ob_start();
        wp_bg_helper(static::$imageId, null, ':before');
        wp_bg_helper(static::$imageId, null, ':after');
        ob_get_clean();

        $uniqueId = $wp_bg_helper_cache? array_keys($wp_bg_helper_cache)[0] : null;
        $uniqueId2 = $wp_bg_helper_cache? array_keys($wp_bg_helper_cache)[1] : null;

        $this->assertCount(2, $wp_bg_helper_cache);
        $this->assertRegExp('/\[data-bg="'.$uniqueId.'"\]:before /', array_values($wp_bg_helper_cache)[0]);
        $this->assertRegExp('/\[data-bg="'.$uniqueId2.'"\]:after /', array_values($wp_bg_helper_cache)[1]);
    }

    /** @test */
    public function it_will_use_the_assigned_selector_if_passed()
    {
        global $wp_bg_helper_cache;

        ob_start();
        wp_bg_helper(static::$imageId, null, '#some-id');
        wp_bg_helper(static::$imageId, null, '.some-class');
        $output = ob_get_clean();

        $this->assertCount(2, $wp_bg_helper_cache);
        $this->assertEquals('', $output);
        $this->assertRegExp('/#some-id /', array_values($wp_bg_helper_cache)[0]);
        $this->assertRegExp('/\.some-class /', array_values($wp_bg_helper_cache)[1]);
    }

    /** @test */
    public function it_will_print_all_bg_styles()
    {
        global $wp_bg_helper_cache;

        ob_start();
        wp_bg_helper(static::$imageId);
        wp_bg_helper(static::$imageId, null, ':before');
        wp_bg_helper(static::$imageId, null, ':after');
        wp_bg_helper(static::$imageId, null, '#some-id');
        wp_bg_helper(static::$imageId, null, '.some-class');
        ob_get_clean();

        ob_start();
        wp_bg_helper_print_styles();
        $output = ob_get_clean();

        $uniqueId = $wp_bg_helper_cache? array_keys($wp_bg_helper_cache)[0] : null;
        $uniqueId2 = $wp_bg_helper_cache? array_keys($wp_bg_helper_cache)[1] : null;
        $uniqueId3 = $wp_bg_helper_cache? array_keys($wp_bg_helper_cache)[2] : null;

        $this->assertRegExp('/\[data-bg="'.$uniqueId.'"\] /', $output);
        $this->assertRegExp('/\[data-bg="'.$uniqueId2.'"\]:before /', $output);
        $this->assertRegExp('/\[data-bg="'.$uniqueId3.'"\]:after /', $output);
        $this->assertRegExp('/#some-id /', $output);
        $this->assertRegExp('/\.some-class /', $output);
    }
}
