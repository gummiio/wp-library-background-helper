<?php

namespace Test\Unit;

use Gummiforweb\WpBackgroundHelper\WpBackgroundHelper;
use Tests\TestCase;
use Tests\WpUploadImage;

class WpBackgroundHelperTest extends TestCase
{
    use WpUploadImage;

    public function setUp()
    {
        parent::setUp();

        add_image_size('safe_full', 1600, null, false);

        WpBackgroundHelper::clearCache();
        WpBackgroundHelper::setScreenMaxWidth(1800);
        WpBackgroundHelper::setThumbnailMaxSize('safe_full');
        WpBackgroundHelper::setBreakpointShorthands([
            'xs' => 320, 'sm' => 640, 'md' => 768, 'lg' => 980, 'xl' => 1200, 'xxl' => 1400
        ]);
        WpBackgroundHelper::setBreakpointPresets([
            'with-value' => [320 => 1, 640 => 2, 768 => 3],
            'with-shorthand' => ['xs' => 1, 'sm' => 2, 'md' => 3, 'lg' => 4],
            'with-value-string' => '320:1, 640:2, 768:3, 980:4+',
            'with-shorthand-string' => 'xs:1, sm:2, md:3, lg:4+'
        ]);
    }

    public function tearDown()
    {
        global $_wp_additional_image_sizes;

        $_wp_additional_image_sizes = [];

        parent::tearDown();
    }

    /** @test */
    public function available_image_size_will_not_include_fixed_height_or_crop()
    {
        WpBackgroundHelper::clearCache();

        add_image_size('test_480', 480, null, false);
        add_image_size('test_900', 900, null, true);
        add_image_size('test_1000', 1000, 500, false);

        new WpBackgroundHelper;

        $availableSizes = WpBackgroundHelper::getStatic('availableThumbnailSizes');

        $this->assertContains('test_480', $availableSizes);
        $this->assertNotContains('test_900', $availableSizes);
        $this->assertNotContains('test_1000', $availableSizes);
    }

    /** @test */
    public function it_will_cache_available_thumbnail_sizes()
    {
        WpBackgroundHelper::clearCache();
        new WpBackgroundHelper;

        add_image_size('test_480', 480, null, false);

        $this->assertNotContains('test_480', WpBackgroundHelper::getStatic('availableThumbnailSizes'));

        WpBackgroundHelper::clearCache();
        new WpBackgroundHelper;

        $this->assertContains('test_480', WpBackgroundHelper::getStatic('availableThumbnailSizes'));
    }

    /** @test */
    public function it_can_setup_breakpoint_shorthands()
    {
        WpBackgroundHelper::clearCache();
        WpBackgroundHelper::setBreakpointShorthands(['lg' => 980]);

        $this->assertArrayHasKey('lg', WpBackgroundHelper::getStatic('breakpointShorthands'));
    }

    /** @test */
    public function it_can_setup_breakpoint_preset()
    {
        WpBackgroundHelper::clearCache();
        WpBackgroundHelper::setBreakpointPresets([
            'double' => [320 => 1, 640 => 2]
        ]);

        $this->assertArrayHasKey('double', WpBackgroundHelper::getStatic('breakpointPresets'));
    }

    /** @test */
    public function it_will_set_all_breakpoint_from_shorthand()
    {
        $unit = (new WpBackgroundHelper(static::$imageId, 'md:2'))->getUnit();

        $this->assertCount(count(WpBackgroundHelper::getStatic('breakpointShorthands')) + 1, $unit->getBreakpoints());
    }

    /** @test */
    public function it_should_setup_global_breakpoints_value_preset_on_initialize()
    {
        $unit = (new WpBackgroundHelper(static::$imageId, 'with-value'))->getUnit();
        $breakpoints = $unit->getBreakpoints();

        $this->assertEquals(1, $breakpoints[320]);
        $this->assertEquals(2, $breakpoints[640]);
        $this->assertEquals(3, $breakpoints[768]);
        $this->assertEquals(1800, $unit->getMaxWidth());
        $this->assertArrayHasKey(1600, $unit->getImageSources());
    }

    /** @test */
    public function it_should_not_include_full_if_max_thumbnail_size_is_set()
    {
        $unit = (new WpBackgroundHelper(static::$imageId, 'with-value'))->getUnit();
        $src = wp_get_attachment_image_src(static::$imageId, 'full');

        $this->assertArrayNotHasKey($src[1], $unit->getImageSources());
    }

    /** @test */
    public function it_should_setup_global_breakpoints_with_shorthand_preset_on_initialize()
    {
        $unit = (new WpBackgroundHelper(static::$imageId, 'with-shorthand'))->getUnit();
        $breakpoints = $unit->getBreakpoints();

        $this->assertEquals(1, $breakpoints[320]);
        $this->assertEquals(2, $breakpoints[640]);
        $this->assertEquals(3, $breakpoints[768]);
        $this->assertEquals(4, $breakpoints[980]);
    }

    /** @test */
    public function it_should_setup_global_breakpoints_with_value_string_preset_on_initialize()
    {
        $unit = (new WpBackgroundHelper(static::$imageId, 'with-value-string'))->getUnit();
        $breakpoints = $unit->getBreakpoints();

        $this->assertEquals(1, $breakpoints[320]);
        $this->assertEquals(2, $breakpoints[640]);
        $this->assertEquals(3, $breakpoints[768]);
        $this->assertEquals(4, $breakpoints[980]);
        $this->assertEquals(4, $breakpoints[1200]);
        $this->assertEquals(4, $breakpoints[1400]);
    }

    /** @test */
    public function it_should_setup_global_breakpoints_with_shorthand_string_on_initialize()
    {
        $unit = (new WpBackgroundHelper(static::$imageId, 'with-shorthand-string'))->getUnit();
        $breakpoints = $unit->getBreakpoints();

        $this->assertEquals(1, $breakpoints[320]);
        $this->assertEquals(2, $breakpoints[640]);
        $this->assertEquals(3, $breakpoints[768]);
        $this->assertEquals(4, $breakpoints[980]);
        $this->assertEquals(4, $breakpoints[1200]);
        $this->assertEquals(4, $breakpoints[1400]);
    }

    /** @test */
    public function it_should_setup_global_breakpoints_with_string_directly_on_initialize()
    {
        $unit = (new WpBackgroundHelper(static::$imageId, '320:1, 640:2, 768:5'))->getUnit();
        $breakpoints = $unit->getBreakpoints();

        $this->assertEquals(1, $breakpoints[320]);
        $this->assertEquals(2, $breakpoints[640]);
        $this->assertEquals(5, $breakpoints[768]);
    }

    /** @test */
    public function it_can_use_space_instead_in_the_breakpoint_string()
    {
        $unit = (new WpBackgroundHelper(static::$imageId, '320:1 640:2 768:5'))->getUnit();
        $breakpoints = $unit->getBreakpoints();

        $this->assertEquals(1, $breakpoints[320]);
        $this->assertEquals(2, $breakpoints[640]);
        $this->assertEquals(5, $breakpoints[768]);
    }

    /** @test */
    public function it_can_set_max_width_from_the_breakpoint_string()
    {
        $unit = (new WpBackgroundHelper(static::$imageId, '320:1, 640:2, 768:5, !1200'))->getUnit();

        $this->assertArrayNotHasKey(1200, $unit->getBreakpoints());
        $this->assertEquals(1200, $unit->getMaxWidth());
    }

    /** @test */
    public function it_will_stop_at_max_width_on_aggrasive_breakpoint_ratio()
    {
        $unit = (new WpBackgroundHelper(static::$imageId, '320:1, 640:2, 768:3+, !1200'))->getUnit();

        $this->assertArrayNotHasKey(1200, $unit->getBreakpoints());
        $this->assertArrayNotHasKey(1400, $unit->getBreakpoints());
    }

    /** @test */
    public function it_will_have_all_available_image_source_loaded_on_initialize()
    {
        WpBackgroundHelper::clearAvailableThunbmailSizes();
        add_image_size('test_480', 480, null, false);
        add_image_size('test_900', 900, null, false);
        add_image_size('test_1000', 1000, null, false);
        $this->regenerateThunbmails(static::$imageId);

        $helper = new WpBackgroundHelper(static::$imageId);
        $imageSources = $helper->getUnit()->getImageSources();

        $this->assertArrayHasKey(480, $imageSources);
        $this->assertArrayHasKey(900, $imageSources);
        $this->assertArrayHasKey(1000, $imageSources);
    }

    /** @test */
    public function it_should_not_wrap_first_query_in_media()
    {
        WpBackgroundHelper::clearAvailableThunbmailSizes();
        add_image_size('test_480', 480, null, false);
        add_image_size('test_640', 640, null, false);
        add_image_size('test_768', 768, null, false);
        add_image_size('test_1024', 1024, null, false);
        $this->regenerateThunbmails(static::$imageId);

        $helper = new WpBackgroundHelper(static::$imageId, 'xs:1, md:1');
        $styles = $helper->getStylesArray('#asdf');

        $this->assertNotRegExp('/^@media screen and \(/', $styles[0]);
    }

    /** @test */
    public function it_should_treat_ratio_as_1_for_breakpoint_if_nothing_passed()
    {
        WpBackgroundHelper::clearAvailableThunbmailSizes();
        add_image_size('test_480', 480, null, false);
        add_image_size('test_640', 640, null, false);
        add_image_size('test_768', 768, null, false);
        add_image_size('test_1024', 1024, null, false);
        $this->regenerateThunbmails(static::$imageId);

        $helper = new WpBackgroundHelper(static::$imageId);
        $styles = $helper->getStylesArray('#asdf');

        $this->assertTrue(count($styles) > 1);
    }
}
