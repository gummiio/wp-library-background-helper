<?php

namespace Test\Unit;

use Gummiforweb\WpBackgroundHelper\WpBackgroundUnit;
use Tests\TestCase;
use Tests\WpUploadImage;

class WpBackgroundUnitTest extends TestCase
{
    use WpUploadImage;

    public function setUp()
    {
        parent::setUp();

        $this->unit = new WpBackgroundUnit;
    }

    /** @test */
    public function it_can_set_background_by_image_id()
    {
        $this->unit->setImage(static::$imageId);

        $this->assertEquals(static::$imageId, $this->unit->getImage());
    }

    /** @test */
    public function it_can_set_background_by_image_object()
    {
        $this->unit->setImage(get_post(static::$imageId));

        $this->assertEquals(static::$imageId, $this->unit->getImage());
    }

    /** @test */
    public function it_can_set_background_by_image_array()
    {
        $this->unit->setImage((array) get_post(static::$imageId));

        $this->assertEquals(static::$imageId, $this->unit->getImage());
    }

    /** @test */
    public function it_can_set_max_width()
    {
        $this->unit->maxWidth(1800);

        $this->assertEquals(1800, $this->unit->getMaxWidth());
    }

    /** @test */
    public function it_can_unset_max_width()
    {
        $this->unit->maxWidth(1800)->maxWidth(null);

        $this->assertEquals(null, $this->unit->getMaxWidth());
    }

    /** @test */
    public function it_can_set_responsive_breakpoint()
    {
        $this->unit->addBreakpoint(768);

        $this->assertArrayHasKey(768, $this->unit->getBreakpoints());
    }

    /** @test */
    public function it_will_not_set_responsive_breakpoint_if_invalid()
    {
        $this->unit->addBreakpoint('wuuut');

        $this->assertArrayNotHasKey('wuuut', $this->unit->getBreakpoints());
    }

    /** @test */
    public function it_can_set_responsive_breakpoint_ratio()
    {
        $this->unit->addBreakpoint(768, .5);

        $this->assertEquals(2, $this->unit->getBreakpoint(768));
    }

    /** @test */
    public function it_can_set_responsive_breakpoint_ratio_by_items_per_row()
    {
        $this->unit->addBreakpoint(768, 2);

        $this->assertEquals(2, $this->unit->getBreakpoint(768));
    }

    /** @test */
    public function it_can_set_responsive_breakpoint_ratio_by_percentage()
    {
        $this->unit->addBreakpoint(768, '50%');

        $this->assertEquals(2, $this->unit->getBreakpoint(768));
    }

    /** @test */
    public function it_will_set_invalid_responsive_breakpoint_ratio_to_1()
    {
        $this->unit->addBreakpoint(768, 'asdf');

        $this->assertEquals(1, $this->unit->getBreakpoint(768));
    }

    /** @test */
    public function it_will_not_set_responsive_breakpoint_ratio_as_0()
    {
        $this->unit->addBreakpoint(768, 0);

        $this->assertArrayNotHasKey(768, $this->unit->getBreakpoints());
        $this->assertNull($this->unit->getBreakpoint(768));
    }

    /** @test */
    public function it_can_add_image_source()
    {
        $this->unit->addImageSource(768, 'http://domain.com/some-image.jpg');

        $this->assertEquals('http://domain.com/some-image.jpg', $this->unit->getImageSource(768));
    }

    /** @test */
    public function it_will_not_add_image_source_if_empty()
    {
        $this->unit->addImageSource(768, '');

        $this->assertEmpty($this->unit->getImageSources());
        $this->assertArrayNotHasKey(768, $this->unit->getImageSources());
        $this->assertNull($this->unit->getImageSource(768));
    }

    /** @test */
    public function it_will_generate_base_if_no_breakpoint_set()
    {
        $this->unit
            ->addImageSource(400, 'http://domain.com/image-400.jpg');

        $results = $this->unit->getResults();

        $this->assertEquals('http://domain.com/image-400.jpg', $results[0]);
    }

    /** @test */
    public function it_will_generate_simple_results()
    {
        $this->unit
            ->addBreakpoint(768)->addBreakpoint(640)->addBreakpoint(320)
            ->addImageSource(1200, 'http://domain.com/image-1200.jpg')
            ->addImageSource(900, 'http://domain.com/image-900.jpg')
            ->addImageSource(800, 'http://domain.com/image-800.jpg')
            ->addImageSource(640, 'http://domain.com/image-640.jpg')
            ->addImageSource(400, 'http://domain.com/image-400.jpg');

        $results = $this->unit->getResults();

        $this->assertNotEmpty($results);
        $this->assertEquals('http://domain.com/image-1200.jpg', $results[768]);
        $this->assertEquals('http://domain.com/image-800.jpg', $results[640]);
        $this->assertEquals('http://domain.com/image-640.jpg', $results[320]);
        $this->assertEquals('http://domain.com/image-400.jpg', $results[0]);
        $this->assertNotContains('http://domain.com/image-900.jpg', $results);
        $this->assertEquals([0, 320, 640, 768], array_keys($results));
    }

    /** @test */
    public function it_will_skip_breakpoint_that_has_the_same_source()
    {
        $this->unit
            ->addBreakpoint(768)->addBreakpoint(640)->addBreakpoint(320)
            ->addImageSource(1200, 'http://domain.com/image-1200.jpg')
            ->addImageSource(800, 'http://domain.com/image-800.jpg');

        $results = $this->unit->getResults();

        $this->assertEquals('http://domain.com/image-1200.jpg', $results[768]);
        $this->assertEquals('http://domain.com/image-800.jpg', $results[0]);
        $this->assertEquals([0, 768], array_keys($results));
    }

    /** @test */
    public function it_will_calculate_breakpoint_based_on_ratio()
    {
        $this->unit
            ->addBreakpoint(1200)
            ->addBreakpoint(768, 4)->addBreakpoint(640, 2)->addBreakpoint(320)
            ->addImageSource(1200, 'http://domain.com/image-1200.jpg')
            ->addImageSource(800, 'http://domain.com/image-800.jpg')
            ->addImageSource(640, 'http://domain.com/image-640.jpg')
            ->addImageSource(400, 'http://domain.com/image-400.jpg')
            ->addImageSource(300, 'http://domain.com/image-300.jpg');

        $results = $this->unit->getResults();

        $this->assertEquals('http://domain.com/image-300.jpg', $results[768]); // 1200/4 = 300
        $this->assertEquals('http://domain.com/image-400.jpg', $results[640]); // 768/2 = 384
        $this->assertEquals('http://domain.com/image-640.jpg', $results[320]);
        $this->assertEquals('http://domain.com/image-400.jpg', $results[0]);
    }

    /** @test */
    public function it_will_not_set_max_width_if_at_least_one_breakpoint_is_wider()
    {
        $this->unit
            ->maxWidth(1500)
            ->addBreakpoint(480)->addBreakpoint(1700)
            ->addImageSource(1500, 'http://domain.com/image-1500.jpg')
            ->addImageSource(1600, 'http://domain.com/image-1600.jpg');

        $results = $this->unit->getResults();

        $this->assertArrayNotHasKey(1500, $results);
    }

    /** @test */
    public function it_will_not_include_max_width_as_break_point()
    {
        $this->unit
            ->maxWidth(1500)
            ->addBreakpoint(1200)->addBreakpoint(480)
            ->addImageSource(500, 'http://domain.com/image-500.jpg')
            ->addImageSource(1500, 'http://domain.com/image-1500.jpg')
            ->addImageSource(1600, 'http://domain.com/image-1600.jpg');

        $results = $this->unit->getResults();

        $this->assertArrayNotHasKey(1500, $results);
        $this->assertNotContains('http://domain.com/image-1600.jpg', $results);
    }

    /** @test */
    public function it_will_calculate_breakpoint_based_on_max_width()
    {
        $this->unit
            ->maxWidth(1800)
            ->addBreakpoint(768, 4)
            ->addImageSource(1200, 'http://domain.com/image-1200.jpg')
            ->addImageSource(640, 'http://domain.com/image-640.jpg');

        $results = $this->unit->getResults();

        $this->assertEquals('http://domain.com/image-640.jpg', $results[768]); // 1800/4 = 450
        $this->assertEquals('http://domain.com/image-1200.jpg', $results[0]);
        $this->assertArrayNotHasKey(1800, $results);
    }
}
