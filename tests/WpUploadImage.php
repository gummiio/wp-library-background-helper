<?php

namespace Tests;

trait WpUploadImage
{
    protected static $imageId;

    public static function wpSetUpBeforeClass($factory)
    {
        $filename = DIR_TESTDATA . '/images/33772.jpg';
        static::$imageId = $factory->attachment->create_upload_object($filename);
    }

    public static function tearDownAfterClass()
    {
        wp_delete_post(static::$imageId, true);
        parent::tearDownAfterClass();
    }

    protected function regenerateThunbmails($id)
    {
        wp_update_attachment_metadata($id, wp_generate_attachment_metadata($id, get_attached_file($id)));
    }
}
