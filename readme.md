# WordPress Plugin - Responsive Background Output Helper

Helper function that output background css with proper media query.

[ ![Bitbucket Pipeline Status for gummiio/wp-library-background-helper](https://bitbucket-badges.atlassian.io/badge/gummiio/wp-library-background-helper.svg)](https://bitbucket.org/gummiio/wp-library-background-helper/overview)

# Step One
In your theme's `functions.php`, add an action to the _wp_footer_ for the plugin to output the generated css.
```
<?php
// functions.php
add_action('wp_footer', 'theme_output_responsive_background_css');
function theme_output_responsive_background_css() {
    wp_bg_helper_print_styles();
}
```

# Step Two
When you are using background on a html element, you can call `wp_bg_helper($attachment)` function.
```
// banner.php
<section class="page-banner" <?php wp_bg_helper(5); ?>>
    ...
</section>
```
_5 is the attachment id, you can also pass a WP_POST object_

# Output
Before your closing `</body>`, It'd generate the media query css that loads the proper image sizes.
```
[data-bg="bg-5a5ed384d38f7"] {
    background-image: url('https://your-domain.com/wp-content/uploads/2017/08/hp-background-480x181.jpg');
}
@media screen and (min-width: 420px) {
    [data-bg="bg-5a5ed384d38f7"] {
        background-image: url('https://your-domain.com/wp-content/uploads/2017/08/hp-background-768x289.jpg');
    }
}
@media screen and (min-width: 768px) {
    [data-bg="bg-5a5ed384d38f7"] {
        background-image: url('https://your-domain.com/wp-content/uploads/2017/08/hp-background-980x369.jpg');
    }
}
@media screen and (min-width: 980px) {
    [data-bg="bg-5a5ed384d38f7"] {
        background-image: url('https://your-domain.com/wp-content/uploads/2017/08/hp-background-1200x451.jpg');
    }
}
@media screen and (min-width: 1200px) {
    [data-bg="bg-5a5ed384d38f7"] {
        background-image: url('https://your-domain.com/wp-content/uploads/2017/08/hp-background-1800x677.jpg');
    }
}
```
